#pragma optimize("", off)

#include <iostream>
#include <tao/pq.hpp>
#include <tao/pq/connection.hpp>
#include <chrono>
#include <cstdio>
#include <unordered_map>
#include <string>
#include <algorithm>
#include "httplib.h"
#include <Windows.h>
#include <regex>

std::string dump_headers(const httplib::Headers& headers)
{
    std::string s;
    char buf[BUFSIZ];

    for (auto it = headers.begin(); it != headers.end(); ++it)
    {
        const auto& x = *it;
        snprintf(buf, sizeof(buf), "%s: %s\n", x.first.c_str(), x.second.c_str());
        s += buf;
    }

    return s;
}

std::string log(const httplib::Request& req, const httplib::Response& res)
{
    std::string s;
    char buf[BUFSIZ];

    s += "================================\n";
    snprintf(buf, sizeof(buf), "%s %s %s", req.method.c_str(), req.version.c_str(), req.path.c_str());
    s += buf;

    std::string query;
    for (auto it = req.params.begin(); it != req.params.end(); ++it)
    {
        const auto& x = *it;
        snprintf(buf, sizeof(buf), "%c%s=%s", (it == req.params.begin()) ? '?' : '&', x.first.c_str(), x.second.c_str());
        query += buf;
    }

    snprintf(buf, sizeof(buf), "%s\n", query.c_str());
    s += buf;
    s += dump_headers(req.headers);
    s += "--------------------------------\n";

    snprintf(buf, sizeof(buf), "%d %s\n", res.status, res.version.c_str());
    s += buf;
    s += dump_headers(res.headers);
    s += "\n";

    if (!res.body.empty())
    {
        s += res.body;
    }

    s += "\n";
    return s;
}

/* Dependencia m�nima: cURL */
static void SendMail(const std::string& data, const std::string& tomail)
{
    std::string command = "curl --ssl-reqd --url \"smtps://smtp.gmail.com:465\" --user \"lordfers@gmail.com:vassljseyflczost\" --mail-from \"lordfers@gmail.com\" --mail-rcpt \"" + tomail + "\" -T -";
    FILE* pipe = _popen(command.c_str(), "w");
    if (pipe)
    {
        fwrite(data.c_str(), sizeof(char), data.size(), pipe);
        _pclose(pipe);
    }
    else
    {
        std::cerr << "Error al entrar al pipeline de curl." << std::endl;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

enum class CLIENT_PACKET_ID
{
    LOGIN = 0,
    CREATE_USER, // Lo usamos para el panel.
    CREATE_PLAN,
    PLANES_PROFILE, // Este paquete lo env�a el cliente para abrir la ventana y que el servidor le devuelva la lista de planes.
    SAVE_REPORT,
    SEND_REPORT,
    GET_USER_REPORTS, // El cliente le pide al servidor que le env�e los reportes del usuario.
    UNKNOWN
};

enum class SERVER_PACKET_ID
{
    LOGGED = 0,
    CREATE_USER, // Lo usamos para el panel.
    CREATED_PROFILE,
    PLANES_PROFILE,
    GET_USER_REPORTS, // El server le env�a los reportes al cliente.
    SHOW_MESSAGE, // Server can show a error/warning/welcom message.
    UNKNOWN
};

struct User {
    uint32_t id;
    uint32_t privilegies;
    uint32_t last_time; //last time LRU para desconectar al usuario y devolverlo al panel inicial.
    std::vector<uint32_t> plans;
    std::vector<uint32_t> reports;
    std::string name;
    std::string lastname;
};

struct TrainingStep
{
    uint32_t time;
    uint32_t speed;
};

struct Training {
    uint32_t id;
    uint32_t idcreator;
    std::string name;
    std::vector<TrainingStep> steps;
};

struct Report {
    uint32_t id;
    uint32_t idcreator;
    std::string data;
};

/* Global data */
std::shared_ptr<tao::pq::connection> conn;
std::unordered_map<std::string, User> m_CacheUsers;
std::vector<Training> m_Trainings;
std::vector<Report> m_Reports;

void RemoveUser(const std::string& name);
User& GetUser(const std::string& name);
void AddDataToUser(User& user, const std::string& username);

std::vector<std::string> split(std::string s, std::string delimiter)
{
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    std::string token;
    std::vector<std::string> res;

    while ((pos_end = s.find(delimiter, pos_start)) != std::string::npos) {
        token = s.substr(pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back(token);
    }

    res.push_back(s.substr(pos_start));
    return res;
}

Training ParseTrainingData(const std::string& name, uint32_t id, uint32_t idcreator, size_t steps, const std::string& times, const std::string& speeds)
{
    Training train;
    train.name = name;
    train.id = id;
    train.idcreator = idcreator;

    auto time_list = split(times, ",");
    auto speed_list = split(speeds, ",");

    size_t min_count_elements = std::min(time_list.size(), speed_list.size());
    train.steps.resize(min_count_elements);

    for (size_t i = 0; i < min_count_elements; ++i)
    {
        train.steps[i].time = atoi(time_list[i].c_str());
        train.steps[i].speed = atoi(speed_list[i].c_str());
    }

    return train;
}

void LoadTrainingData()
{
    std::cout << "Cargando planes...\n";
    const auto db_plans_data = conn->execute("SELECT * FROM plans");
    for (size_t i = 0; i < db_plans_data.rows_affected(); ++i)
    {
        const auto& db_plan = db_plans_data.at(i);
        const std::string& name = db_plan.at("name").as<std::string>();
        const std::string& times = db_plan.at("times").as<std::string>();
        const std::string& speeds = db_plan.at("speeds").as<std::string>();
        uint32_t id = db_plan.at("id").as<uint32_t>();
        uint32_t idcreator = db_plan.at("idcreator").as<uint32_t>();
        size_t steps = db_plan.at("steps").as<size_t>();

        Training temp_training = ParseTrainingData(name, id, idcreator, steps, times, speeds);
        m_Trainings.push_back(temp_training);
        std::cout << "Nombre del plan: " << temp_training.name << " con id: " << temp_training.id << std::endl;
    }
}

void LoadReportData()
{
    std::cout << "Cargando reportes...\n";
    const auto db_reports_data = conn->execute("SELECT * FROM reports");
    for (size_t i = 0; i < db_reports_data.rows_affected(); ++i)
    {
        const auto& db_report = db_reports_data.at(i);
        const std::string& data = db_report.at("data").as<std::string>();
        uint32_t id = db_report.at("id").as<uint32_t>();
        uint32_t idcreator = db_report.at("idcreator").as<uint32_t>();

        Report report;
        report.id = id;
        report.idcreator = idcreator;
        report.data = data;

        m_Reports.push_back(report);
        std::cout << "Reporte: " << report.id << " - Data: " << report.data << std::endl;
    }
}

bool CheckIsEmail(const std::string& email)
{
    const std::regex email_regex("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}\\b");
    return std::regex_match(email, email_regex);
}

//TODO:
void AddTrainingToUser(const std::string& name)
{
    //actualizarlo en el Cache
}

void AddPlanToUser(const std::string& name)
{
    //actualizarlo en el Cache
}

void CreateNewTraining()
{
    //agregarlo al usuario idcreator si el creador no es un m�dico
}

void CreateNewPlan()
{
    //agregarlo al usuario idcreator si el creador no es un m�dico
}

void AddNewReportToUser(const std::string& name, const std::string& data)
{
    auto& refuser = m_CacheUsers[name];

    // Nos aseguramos la copia para C++11:
    std::string copy_data = std::string(data.c_str());

    // Hay un problem para que los caracteres los entienda como UTF-8, est� tomando wchar el driver, as� que voy a hacer esto mientras tanto:
    //copy_data.erase(std::remove(copy_data.begin(), copy_data.end(), '\n'), copy_data.end());
    //copy_data.erase(std::remove(copy_data.begin(), copy_data.end(), ':'), copy_data.end());

    std::replace(copy_data.begin(), copy_data.end(), '�', 'a');
    std::replace(copy_data.begin(), copy_data.end(), '�', 'e');
    std::replace(copy_data.begin(), copy_data.end(), '�', 'i');
    std::replace(copy_data.begin(), copy_data.end(), '�', 'o');
    std::replace(copy_data.begin(), copy_data.end(), '�', 'u');

    std::replace(copy_data.begin(), copy_data.end(), '�', 'A');
    std::replace(copy_data.begin(), copy_data.end(), '�', 'E');
    std::replace(copy_data.begin(), copy_data.end(), '�', 'I');
    std::replace(copy_data.begin(), copy_data.end(), '�', 'O');
    std::replace(copy_data.begin(), copy_data.end(), '�', 'U');

    uint32_t userid = refuser.id;
    const auto db_report_id = conn->execute("INSERT INTO public.reports (idcreator, data) VALUES($1::bigint, $2::character varying) returning id; ", userid, copy_data.c_str());
    uint32_t reportid = db_report_id.at(0).as<uint32_t>();
    if (reportid > 0)
    {
        Report report;
        report.data = copy_data;
        report.id = reportid;
        report.idcreator = userid;
        m_Reports.push_back(report);
        refuser.reports.push_back(reportid);

        std::string report_format = std::to_string(refuser.reports.at(0));

        for (size_t i = 1; i < refuser.reports.size(); ++i)
        {
            report_format += "," + std::to_string(refuser.reports.at(i));
        }

        // Estamos agregando ",n+1,n+2,n+m" : "4,5,6"
        conn->execute("UPDATE public.users SET reports = $1::character varying WHERE id = $2; ", report_format.c_str(), userid);
    }
}

std::string MakeUserHash(const std::string& name)
{
    return name; //TODO: hash
}

std::vector<Training> GetTrainingByUser(const std::string& name)
{
    // Mape� mal algunos valores as� que para no andar cambiando las tablas o el formato, hacemos esto y fue:
    std::vector<Training> trainings;
    if (m_CacheUsers.find(name) != m_CacheUsers.end())
    {
        for (const auto& training : m_Trainings)
        {
            // If contains plan in training cache, we send them:
            auto plans = m_CacheUsers[name].plans;
            for (auto plan : plans)
            {
                if (training.id == plan)
                {
                    trainings.push_back(training);
                }
            }
        }
    }

    return trainings;
}

std::vector<Report> GetReportsByUser(const std::string& name)
{
    std::vector<Report> reports;
    User userReport;

    bool hasSession = true;
    if (m_CacheUsers.find(name) == m_CacheUsers.end())
    {
        hasSession = false;
    }

    userReport = GetUser(name);

    // Check if user was added.
    if (m_CacheUsers.find(name) != m_CacheUsers.end())
    {
        for (const auto& report : m_Reports)
        {
            // If contains plan in report cache, we send them:
            auto user_reports = m_CacheUsers[name].reports;
            for (auto reportid : user_reports)
            {
                if (report.id == reportid)
                {
                    reports.push_back(report);
                }
            }
        }
    }

    if (!hasSession)
    {
        RemoveUser(name);
    }

    return reports;
}

void AddUser(const std::string& name, User user)
{
    /* If we found it, erase. */
    RemoveUser(name);
    user.last_time = 0;
    AddDataToUser(user, name);
    m_CacheUsers.insert(std::pair<std::string, User>(name, user));//m_CacheUsers[name] = user;
}

void RemoveUser(const std::string& name)
{
    if (m_CacheUsers.find(name) != m_CacheUsers.end())
    {
        m_CacheUsers.erase(name);
    }
}

User& GetUser(const std::string& name)
{
    User user;
    if (m_CacheUsers.find(name) == m_CacheUsers.end())
    {
        user.privilegies = NULL;
        AddUser(name, user);
    }

    return m_CacheUsers.at(name);
}

void AddDataToUser(User& user, const std::string& username)
{
    const auto db_user_id = conn->execute("SELECT id FROM users WHERE username = $1", username.c_str());
    const auto db_user_privilegies = conn->execute("SELECT privilegies FROM users WHERE username = $1", username.c_str());
    const auto db_user_plans = conn->execute("SELECT plans FROM users WHERE username = $1", username.c_str());
    const auto db_user_reports = conn->execute("SELECT reports FROM users WHERE username = $1", username.c_str());
    const auto db_user_name = conn->execute("SELECT name FROM users WHERE username = $1", username.c_str());
    const auto db_user_lastname = conn->execute("SELECT lastname FROM users WHERE username = $1", username.c_str());

    user.id = db_user_id.at(0).as<uint32_t>();
    user.privilegies = db_user_privilegies.at(0).as<uint32_t>();
    user.last_time = 0;
    user.name = db_user_name.at(0).as<std::string>();
    user.lastname = db_user_lastname.at(0).as<std::string>();

    // Vamos a hacerlo r�pido:
    std::vector<std::string> plans = split(db_user_plans.at(0).as<std::string>(), ",");
    std::vector<std::string> reports = split(db_user_reports.at(0).as<std::string>(), ",");

    for (auto plan : plans)
    {
        user.plans.push_back(atoi(plan.c_str()));
    }

    for (auto report : reports)
    {
        user.reports.push_back(atoi(report.c_str()));
    }
}

/* Lo agrego temporal as�, por alg�n motivo est� fallando un retorno de un registro de las consultas. */
bool UserExists(const std::string& name)
{
    const auto db_user_password = conn->execute("SELECT password FROM users WHERE username = $1", name.c_str());
    //const std::string& dbpassword = db_user_password.at(0).as<std::string>();
    //return (dbpassword.size() > 0);
    return (db_user_password.rows_affected() > 0);
}

void write_data(httplib::Response& res, const uint8_t packet_id, const std::string& data)
{
    res.set_content(std::to_string(packet_id) + "," + data, "text/plain");
}

void write_show_message(const std::string& message, httplib::Response& res)
{
    write_data(res, static_cast<uint8_t>(SERVER_PACKET_ID::SHOW_MESSAGE), message);
}

void handle_data(const uint8_t packet_id, const httplib::Request& req, httplib::Response& res)
{
    switch (static_cast<CLIENT_PACKET_ID>(packet_id))
    {
    case CLIENT_PACKET_ID::LOGIN:
    {
        const std::string& username = req.get_param_value("username");
        const std::string& password = req.get_param_value("password");
        const auto db_user_password = conn->execute("SELECT password FROM users WHERE username = $1", username.c_str());

        const std::string& dbpassword = db_user_password.at(0).as<std::string>();
        if (strcmp(password.c_str(), dbpassword.c_str()) == 0)
        {
            User user;

            // Add and update cache:
            AddUser(username, user);

            // Send packet to login in App.
            write_data(res, static_cast<uint8_t>(SERVER_PACKET_ID::LOGGED), std::to_string(m_CacheUsers[username].privilegies) + "," + m_CacheUsers[username].name + "," + m_CacheUsers[username].lastname);
        }
        break;
    }

    case CLIENT_PACKET_ID::CREATE_USER:
    {
        const std::string& username = req.get_param_value("username");
        const std::string& password = req.get_param_value("password");
        const std::string& name = req.get_param_value("name");
        const std::string& lastname = req.get_param_value("lastname");

        if (!CheckIsEmail(username))
        {
            write_show_message("El usuario debe ser un correo electronico.", res);
            break;
        }

        if (!UserExists(username))
        {
            conn->execute("INSERT INTO public.users (username, password, name, lastname, privilegies, plans, reports) VALUES($1::character varying, $2::character varying, $3::character varying, $4::character varying, '0'::bigint, '1'::character varying, '1'::character varying) returning id; ", username.c_str(), password.c_str(), name.c_str(), lastname.c_str());
            write_data(res, static_cast<uint8_t>(SERVER_PACKET_ID::CREATE_USER), "El usuario fue creado exitosamente.");
        }
        else
        {
            write_data(res, static_cast<uint8_t>(SERVER_PACKET_ID::CREATE_USER), "El usuario ya existe.");
        }

        break;
    }

    case CLIENT_PACKET_ID::CREATE_PLAN:
    {
        const std::string& username = req.get_param_value("username");

        break;
    }

    case CLIENT_PACKET_ID::SAVE_REPORT:
    {
        const std::string& username = req.get_param_value("username");
        const std::string& equipment = req.get_param_value("equipment");
        const std::string& date = req.get_param_value("date");
        const std::string& speed = req.get_param_value("speed");// NO VA, DEBER�A IR EL ID DEL PLAN.
        const std::string& time = req.get_param_value("time");// NO VA, DEBER�A IR EL ID DEL PLAN.
        const std::string& weight = req.get_param_value("weight");
        const std::string& time2 = req.get_param_value("time2");
        const std::string& time3 = req.get_param_value("time3");
        const std::string& type = req.get_param_value("type"); // MAIL

        // No encontr� al usuario.
        if (m_CacheUsers.find(username) == m_CacheUsers.end())
            break;

        std::string report = "Estimado " + username + " el reporte de tu entrenamiento es el siguiente \n";
        report += "Equipo N: " + equipment + "\n";
        report += "Fecha: " + date + "\n";
        report += "Velocidad " + speed + " unidades.\n";
        report += "Tiempo: " + time + " minutos.\n";
        report += "Peso: " + weight + " kg.\n";
        report += "Duraci�n total: " + time3 + " minutos.\n";
        report += "Tiempo restante: " + time2 + " minutos.\n";
        report += "Atte. RealWalker Staff.";

        if (!strcmp(type.c_str(), "1"))
        {
            std::string data = "From: RealWalker <lordfers@gmail.com>\nTo: " + username + " <" + username + ">\nSubject: Reporte de entrenamiento.\nDate: Mon, 28 Jun 2022 08:45:16\n\n";
            data += report;
            SendMail(data, username);
        }
        else
        {
            AddNewReportToUser(username, report);
        }

        break;
    }

    case CLIENT_PACKET_ID::SEND_REPORT:
    {
        // PODEMOS USAR ESTE PAQUETE PARA OTRA COSA, YA QUE ESTE PAQUETE SE COMPARTE CON EL DE ARRIBA USANDO EL TYPE.

        break;
    }

    case CLIENT_PACKET_ID::PLANES_PROFILE:
    {
        const std::string& username = req.get_param_value("username");
        std::vector<Training> trainings = GetTrainingByUser(username);

        /* Parece trivial que deber�amos pasar los datos como vienen de la DB a la aplicaci�n del MIT,
        *  pero si no llega a estar bien asignada la informaci�n, parsearla bien desde MIT ser�a un trabajo muy complejo
        *  de depurar e implementar, as� que ac� nos aseguramos que la informaci�n llegue en formato CSV como corresponde.
        */

        std::string packet_data = "";//std::to_string(trainings.size()) + ",";
        for (auto& training : trainings)
        {
            packet_data += training.name + "," + std::to_string(training.steps.size()) + ",";

            //std::string step_data = std::to_string(training.steps.size()) + ",";
            for (auto& step : training.steps)
            {
                packet_data += std::to_string(step.time) + "," + std::to_string(step.speed) + ",";
                //step_data += std::to_string(step.time) + "," + std::to_string(step.speed) + ",";
            }

            packet_data[packet_data.size() - 1] = '\n';
            // Enviamos un entrenamiento por cada paquete a la vez, as� es m�s f�cil de parsearlo en MIT.
            //write_data(res, static_cast<uint8_t>(SERVER_PACKET_ID::PLANES_PROFILE), training.name + "," + step_data);
        }
        std::string final_data = packet_data.substr(0, packet_data.size() - 1);
        write_data(res, static_cast<uint8_t>(SERVER_PACKET_ID::PLANES_PROFILE), final_data);
        break;
    }

    case CLIENT_PACKET_ID::GET_USER_REPORTS:
    {
        const std::string& username = req.get_param_value("username");
        if (!UserExists(username))
        {
            write_show_message("Usuario inexistente.", res);
            break;
        }

        std::vector<Report> reports = GetReportsByUser(username);
        if (reports.size() <= 0)
        {
            write_show_message("Este usuario no posee reportes a�n.", res);
            break;
        }

        std::string packet_data = "";
        for (const auto& report : reports)
        {
            std::string temp_data = report.data;
            std::replace(temp_data.begin(), temp_data.end(), '\n', ' ');
            packet_data += temp_data + "\n";
        }

        std::string final_data = packet_data.substr(0, packet_data.size() - 1);
        write_data(res, static_cast<uint8_t>(SERVER_PACKET_ID::GET_USER_REPORTS), final_data);
        break;
    }

    default:
        break;
    }
}

int main(void) {

    httplib::Server svr;

    if (!svr.is_valid()) {
        printf("server has an error...\n");
        return -1;
    }

    svr.Post("/multipart", [&](const auto& req, auto& res) {
        auto is_multipart_format = req.has_file("packetid");
        std::string packet_data = "";

        if (is_multipart_format)
        {
            const auto& packet_content = req.get_file_value("packet_id");
            packet_data = packet_content.content;
            /* We doesn't support multipart-format because MIT App Inventor doesn't support. */
        }
        else
        {
            packet_data = req.get_param_value("packetid");
        }

        if (strlen(packet_data.c_str()) > 0)
        {
            uint8_t packet_id = atoi(packet_data.c_str());
            handle_data(packet_id, req, res);
        }
        else
        {
            res.set_content("Invalid PacketID...\n", "text/plain");
            res.set_redirect("/error");
        }

        });

    svr.Get("/", [=](const httplib::Request& /*req*/, httplib::Response& res) {
        res.set_redirect("/hi");
        });

    svr.Get("/hi", [](const httplib::Request& /*req*/, httplib::Response& res) {
        res.set_content("Data load!\n", "text/plain");
        });

    svr.Post("/login", [&](const auto& req, auto& res) {
        auto size = req.files.size();
        auto ret = req.has_file("name1");
        const auto& file = req.get_file_value("name1");
        printf("llega\n");
        });

    svr.Get("/data", [](const httplib::Request& /*req*/, httplib::Response& res) {
        res.set_content("Data load!\n", "text/plain");
        });

    svr.Get("/slow", [](const httplib::Request& /*req*/, httplib::Response& res) {
        std::this_thread::sleep_for(std::chrono::seconds(2));
        res.set_content("Slow...\n", "text/plain");
        });

    svr.Get("/dump", [](const httplib::Request& req, httplib::Response& res) {
        res.set_content(dump_headers(req.headers), "text/plain");
        });

    svr.Get("/stop",
        [&](const httplib::Request& /*req*/, httplib::Response& /*res*/) { svr.stop(); });

    svr.set_error_handler([](const httplib::Request& /*req*/, httplib::Response& res) {
        const char* fmt = "<p>Error Status: <span style='color:red;'>%d</span></p>";
        char buf[BUFSIZ];
        snprintf(buf, sizeof(buf), fmt, res.status);
        res.set_content(buf, "text/html");
        });

    svr.set_logger([](const httplib::Request& req, const httplib::Response& res) {
        printf("%s", log(req, res).c_str());
        });

    const char* cfgServiceEndpoint = "http://0.0.0.0:8080/v1/";
    const char* cfgDatabaseConnectionString = "host=localhost dbname=realwalker user=postgres password=123456";

    if (const char* env = std::getenv("RW_DB_CONNECTION_STRING"))
    {
        cfgDatabaseConnectionString = env;
    }

    try
    {
        std::cout << "Probando conectar a la DB\n";
        conn = tao::pq::connection::create(cfgDatabaseConnectionString);
        // query data
        const auto users = conn->execute("SELECT username, id FROM users WHERE id = $1", 2);

        // iterate and convert results
        for (const auto& row : users) {
            std::cout << row["username"].as< std::string >() << " is "
                << row["id"].as< unsigned >() << " id register.\n";
        }

        LoadTrainingData();
        LoadReportData();
    }
    catch (...)
    {
        printf("Error connecting DB.\n");
    }

    svr.set_tcp_nodelay(true);
    svr.listen("0.0.0.0", 7667);

    return 0;
}