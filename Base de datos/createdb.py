import psycopg2

# Connecting to Postgre
conn_server = psycopg2.connect(
    host="localhost",
    user="postgres",
    password="123456"
)

cur_server = conn_server.cursor()

# Create "realwalker" if not exists
cur_server.execute("CREATE DATABASE IF NOT EXISTS realwalker")
conn_server.commit()
cur_server.close()
conn_server.close()

# Now connect to "realwalker" db
conn = psycopg2.connect(
    host="localhost",
    dbname="realwalker",
    user="postgres",
    password="123456"
)

cur = conn.cursor()

# Create tables and fields if they doesn't exist:
# users
cur.execute("""
CREATE TABLE IF NOT EXISTS users (
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR UNIQUE,
    password VARCHAR,
    name VARCHAR,
    lastname VARCHAR,
    privilegies BIGINT DEFAULT 0,
    plans VARCHAR DEFAULT '',
    reports VARCHAR DEFAULT ''
)
""")

# plans
cur.execute("""
CREATE TABLE IF NOT EXISTS plans (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR,
    idcreator BIGINT,
    steps BIGINT,
    times VARCHAR DEFAULT '',
    speeds VARCHAR DEFAULT ''
)
""")

# reports
cur.execute("""
CREATE TABLE IF NOT EXISTS reports (
    id BIGSERIAL PRIMARY KEY,
    idcreator BIGINT,
    data VARCHAR DEFAULT ''
)
""")

# Send and close
conn.commit()
cur.close()
conn.close()

print("�Base de datos y tablas creadas exitosamente!")