# RealWalker


## Integrantes
Ledesma Yanina
Quinteros Fernando
Villalba Rodrigo


## Resumen del proyecto
Real Walker se presenta como un dispositivo robotico diseñado para mejorar la movilidad de personas con afecciones neurologicas en las extremidades inferiores. Este proyecto integra hardware y software, utilizando un Arduino Mega 2560, un variador de velocidad G110 y una aplicacion Android desarrollada con MIT App Inventor.


## Caracteristicas principales
* Control total del Real Walker mediante la aplicación Android.
* Entrenamientos manuales y con planes predefinidos.
* Recopilación de datos de peso del paciente a través de un potenciómetro.
* Comunicación Bluetooth entre la aplicación y el Arduino para un control directo.
* Modo offline para garantizar la continuidad en la recopilación de datos en ausencia de conexión Wi-Fi.
* Generación de informes detallados sobre el progreso del paciente.
* Integración de un circuito para controlar un cooler y garantizar la comodidad durante el ejercicio.

# Demostraciones de Funcionalidades de la Aplicación

En este repositorio se encuentran demostraciones en video de diferentes funcionalidades de la aplicación. A continuación se detallan las demostraciones disponibles:

## 1. Crear Cuenta

- **Descripción**: Demostración de creación de una cuenta nueva. Verificación de creación de cuenta en base de datos.
- **Enlace**: [YouTube](https://www.youtube.com/watch?v=_444UwiuMI4)

## 2. Entrenamiento Offline

- **Descripción**: Demostración de entrenamiento en modo offline. Obtención de peso. Validación de tiempo mínimo. Cambios de velocidades cooler. Se guarda archivo txt dentro de la app.
- **Enlace**: [YouTube](https://www.youtube.com/watch?v=BpcpcsZPj3M)

## 3. Envío de Reportes Offline

- **Descripción**: Demostración de envío de reportes. Una vez logueado en la aplicación, se detecta entrenamientos offline y se consulta para subir reportes.
- **Enlace**: [YouTube](https://www.youtube.com/watch?v=HCXF-fkUvtY)

## 4. Entrenamiento con Plan

- **Descripción**: Demostración de entrenamiento online con cuenta, accediendo a los planes predefinidos. Entrenamiento por etapas de plan. Cambios de velocidades cooler.
- **Enlace**: [YouTube](https://www.youtube.com/watch?v=8LFw0HCG4kQ)

## 5. Entrenamiento Manual

- **Descripción**: Demostración de entrenamiento manual online con cuenta registrada, seteando los minutos y velocidades manualmente. Validación de tiempo máximo. Cambios de velocidades cooler.
- **Enlace**: [YouTube](https://www.youtube.com/watch?v=PM2GwDYqqUQ)

## 6. Reportes Médicos

- **Descripción**: Demostración de usuario con perfil MEDICO, el cual accede a los reportes de los pacientes por búsqueda.
- **Enlace**: [YouTube](https://www.youtube.com/watch?v=zNCDILbWqVc)

## 7. Configuración

- **Descripción**: Demostración de acceso a la configuración de la aplicación. Bluetooth y potenciometro.
- **Enlace**: [YouTube](https://www.youtube.com/watch?v=5jkwN0usrDc)

## Dependencias Externas del Servidor

El proyecto utiliza las siguientes dependencias externas:

- [taoPQ](https://github.com/taocpp/taopq): Biblioteca para acceder a bases de datos PostgreSQL desde C++.  
  **Licencia:** Boost Software 1.0

- [httplib](https://github.com/yhirose/cpp-httplib): Biblioteca ligera para manejar solicitudes y respuestas HTTP en C++.  
  **Licencia:** MIT

## Iniciar el Servidor

Antes de iniciar el servidor, asegúrate de haber configurado la base de datos ejecutando el script `createdb.py` ubicado en la carpeta `Base de Datos`. Este script se encargará de crear la base de datos y las tablas necesarias si aún no existen.

Para ejecutar el script `createdb.py`, puedes utilizar los siguientes comandos:

```bash
cd Base de Datos\
```

```bash
python createdb.py
```

Una vez que la base de datos esté configurada correctamente, puedes iniciar el servidor desde el ejecutable. Asegúrate de haber compilado el servidor correctamente antes de intentar iniciarlo, compilando las referencias anteriormente indicadas; en su defecto se encuentran las dependencias ya precompiladas para MSVC16 y MSVC17 (respectivas de Microsoft Visual Studio 2019 y 2022).

Después de ejecutar estos pasos, el servidor estará listo para aceptar conexiones y manejar solicitudes según la configuración proporcionada.
