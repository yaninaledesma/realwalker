char valor_led;  //Variable para indicar que llega una orden

//Variables asociadas al Pote que va a controlar
int valor_pote = 0;    // Variable donde almacenaremos el valor del potenciómetro
float voltaje = 0;  // Variable para almacenar el voltaje
String readString;
int salida_PWM =  3;// Declaramos el pin 3 como salida PWM
int salida_LED = 22;// Declaramos el pin 22 como salida del LED
float valor_minimo = 0;
float valor_maximo = 0;
int valor_leido = 0;
int peso_base = 100; 
float valor = 0;


void setup() 
{
  Serial.begin(9600);
  pinMode(salida_LED, OUTPUT);
  pinMode (salida_PWM, OUTPUT);
}

void loop() {
  char dato = Serial.read();
  switch(dato) {
    case 'A' :encender_led();
    break;
    case 'B' :apagar_led();
    break;
    case 'C' :potenciometro_valor_minimo();
    break;
    case 'D' :velocidad();
    break;
    case 'E' :reset();
    break;
    case 'F' :potenciometro_valor_maximo();
    break;
    case 'G' :peso_obtenido();
    break;
  }
}

void reset()
{
  digitalWrite(salida_LED, LOW); //apagamos led
  valor_pote = 0; //ponemos pote en 0
  analogWrite(salida_PWM, 0); //ponemos cooler en 0;
}
void encender_led()
{
  digitalWrite(salida_LED, HIGH);  //Enciende el LED
}

void apagar_led()
{
  digitalWrite(salida_LED, LOW);
}

void velocidad()
{
  delay(15);
  while (Serial.available())
  {

    char C = Serial.read(); 
    readString += C;  
    
  }
  if (readString.length()>0)
  {
    switch(readString.toInt()) {
    case 0 :
    analogWrite(salida_PWM, 0);
    readString = ""; //limpiamos write de entrada
    break;
    case 1 :
    analogWrite(salida_PWM, 50);
    readString = "";
    break;
    case 2 :
    analogWrite(salida_PWM, 60);
    readString = "";
    break;
    case 3 :
    analogWrite(salida_PWM, 80);
    readString = "";
    break;
    case 4 :
    analogWrite(salida_PWM, 100);
    readString = "";
    break;
    case 5 :
    analogWrite(salida_PWM, 127.5);
    readString = "";
    break;
    case 6 :
    analogWrite(salida_PWM, 153);
    readString = "";
    break;
    case 7 :
    analogWrite(salida_PWM, 178.5);
    readString = "";
    break;
    case 8 :
    analogWrite(salida_PWM, 204);
    readString = "";
    break;
    case 9 :
    analogWrite(salida_PWM, 229.5);
    readString = "";
    break;
    case 10 :
    analogWrite(salida_PWM, 255);
    readString = "";
    break;
    
    default:
    analogWrite(salida_PWM, 0);
    readString = "";
    break;
  }
  }
}

void potenciometro_valor_minimo()
{
  valor_minimo = analogRead(A0);        // Leemos del pin A0 valor
  Serial.print("Valor minimo establecido: ");
  Serial.println(valor_minimo);
}

void potenciometro_valor_maximo() {
  valor_maximo = analogRead(A0);
  Serial.print("Valor maximo establecido: ");    
  Serial.println(valor_maximo);   
}

void peso_obtenido() {
  delay(1000);
  valor_leido = analogRead(A0);
  float valor_1 = (float) (valor_leido-valor_minimo);
  float valor_2 = (float) (valor_maximo-valor_minimo);
  if(valor_2 != 0) 
  {
    valor = valor_1/valor_2;
  } else
  {
    valor = 1;
  }
  int peso = peso_base*valor; 
  Serial.println(peso);   
}
